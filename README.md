Patches for Openair-CN in order to work with remote controller
======================================
    0001-Initial-configuration-section-and-parsing-for-sgw-re.patch
    0002-UE-IP-allocation-increments-one-by-one.patch
    0003-Fix-UE-IP-allocation-to-have-subnet-mask-actually-wo.patch
    0004-Send-HTTP-post-towards-remote-controller-to-delete-b.patch
    0005-Send-HTTP-post-towards-remote-controller-to-add-bear.patch

#### Specific commits are required

    cd ~/openair-cn
    git reset --hard ccc93f50c8c5769de5b7720f18c9e5cec33baa2e

#### Copy all the above files to the root of openair-cn folder and apply, e.g.

    cp *.patch ~/openair-cn/
    git am 0001-Initial-configuration-section-and-parsing-for-sgw-re.patch
    git am 0002-UE-IP-allocation-increments-one-by-one.patch
    git am 0003-Fix-UE-IP-allocation-to-have-subnet-mask-actually-wo.patch
    git am 0004-Send-HTTP-post-towards-remote-controller-to-delete-b.patch
    git am 0005-Send-HTTP-post-towards-remote-controller-to-add-bear.patch

#### Example configuration in S-GW section (spgw.conf)

    
    S-GW :
    {
        NETWORK_INTERFACES :
        {
            # other configurations above skipped here
            
            # external controller for managing context of UE and its user plane
            SGW_REMOTE_CONTROLLER_ENABLED = "yes";
            SGW_REMOTE_CONTROLLER_IPV4_ADDRESS = "192.168.12.242";
            SGW_REMOTE_CONTROLLER_PORT = 9999;
        }
    };
